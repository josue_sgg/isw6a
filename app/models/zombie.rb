class Zombie < ApplicationRecord
    has_many :brains, dependent: :destroy
    belongs_to :user
     validates :bio,length: {maximum:100}
    validates :name, presence:true
    validates :email, format: { with:  /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i}
     validates :age,numericality:{only_integer: true,message: "solo numeros enteros"}

    mount_uploader :avatar, AvatarUploader
end
